package az.ubank;

import az.ubank.bst.BST;
import az.ubank.hashmap.CustomHashMap;

public class Main {
    public static void main(String[] args) {
        BST binaryTree = BST.createBinaryTree();
        System.out.println("------------------BST Descending Order------------------");
        binaryTree.soutDesc(binaryTree.root);
        System.out.println("\n------------------BST Ascending Order------------------");
        binaryTree.soutAsc(binaryTree.root);

        System.out.println("\n------------------HashMap------------------");

        CustomHashMap<String,String>map= new CustomHashMap<>();
        map.put("1","A");
        map.put("2","B");
        map.put("3","C");
        map.put("4","D");
        map.put("5","E");
        map.put("6","F");
        System.out.println(map.get("1"));
        System.out.println(map.remove("1"));
        map.printAll();


    }
}